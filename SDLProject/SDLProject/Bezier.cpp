#include "Bezier.h"
#include "FillColor.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
    CircleFill(p1.x,p1.y,5,ren,{0,255,0,255});
    CircleFill(p2.x,p2.y,5,ren,{0,255,0,255});
    CircleFill(p3.x,p3.y,5,ren,{0,255,0,255});
	float t = 0;
	Vector2D v;
	while (t <= 1)
	{
		float t1 = (1 - t)*(1 - t);
		float t2 = 2 * (1 - t) * t;
		float t3 = t * t;
		v.x = t1 * p1.x + t2 * p2.x + t3 * p3.x;
		v.y = t1 * p1.y + t2 * p2.y + t3 * p3.y;
		SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
		//cout << int(v.x+0.5) <<" "<< int(v.y+0.5) << endl;
		SDL_RenderDrawPoint(ren, int(v.x+0.5), int(v.y+0.5));
		t = t + 0.001;
	}
	
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
    CircleFill(p1.x,p1.y,5,ren,{0,255,0,255});
    CircleFill(p2.x,p2.y,5,ren,{0,255,0,255});
    CircleFill(p3.x,p3.y,5,ren,{0,255,0,255});
    CircleFill(p4.x,p4.y,5,ren,{0,255,0,255});
	float t = 0;
	Vector2D v;
	while (t <= 1)
	{
		float t1 = (1 - t)*(1 - t)*(1 - t);
		float t2 = 3 * (1 - t)*(1 - t)*t;
		float t3 = 3 * (1 - t) * t * t;
		float t4 = t * t * t;
		v.x = t1 * p1.x + t2 * p2.x + t3 * p3.x + t4 * p4.x;
		v.y = t1 * p1.y + t2 * p2.y + t3 * p3.y + t4 * p4.y;
		SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
		//cout << int(v.x + 0.5) << " " << int(v.y + 0.5) << endl;
		SDL_RenderDrawPoint(ren, int(v.x + 0.5), int(v.y + 0.5));
		t = t + 0.001;
	}
}
