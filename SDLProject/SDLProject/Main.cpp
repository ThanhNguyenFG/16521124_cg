#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "FillColor.h"
#include "Line.h"
using namespace std;

const int WIDTH = 1000;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 50, 50, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE
	SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
	Vector2D A(100, 100);
	Vector2D B(200, 700);
	Vector2D C(500, 600);
	Vector2D D(600, 300);
	/*Midpoint_Line(A.x, A.y, C.x, C.y, ren);
	Midpoint_Line(A.x, A.y, B.x, B.y, ren);
	Midpoint_Line(D.x, D.y, B.x, B.y, ren);
	Midpoint_Line(C.x, C.y, D.x, D.y, ren);
	SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
	Uint32 pixel_format = SDL_GetWindowPixelFormat(win);*/
	SDL_Color fillColor = { 255,0,0 ,255};
	SDL_Color boundaryColor = { 0,255,0 ,255};
	//Vector2D startPoint (15, 25);
	//BoundaryFill4(win, startPoint, pixel_format, ren, fillColor, boundaryColor);
	//TriangleFill(A, B, C, ren, fillColor);
	//DrawCurve3(ren, A, B, C, D);
	//FillIntersectionEllipseCircle(400, 400, 300, 100, 400, 400, 200, ren, fillColor);
	//FillIntersectionTwoCircles(200, 200, 400, 400, 400, 200, ren, fillColor);
	DrawCurve3(ren, A, B, C, D);
	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	bool running = true;
	int x, y;
	Vector2D t;
	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{
			//If the user has Xed out the window
			if (event.type == SDL_MOUSEBUTTONDOWN) {
				SDL_GetMouseState(&x, &y);
				if (sqrt((x - A.x)*(x - A.x) + (y - A.y)*(y - A.y)) <= 25) t = A; else
				if (sqrt((x - B.x)*(x - B.x) + (y - B.y)*(y - B.y)) <= 25) t = B; else
				if (sqrt((x - C.x)*(x - C.x) + (y - C.y)*(y - C.y)) <= 25) t = C; else
				if (sqrt((x - D.x)*(x - D.x) + (y - D.y)*(y - D.y)) <= 25) t = D; 
			}


			if (event.type == SDL_MOUSEMOTION)
			{
				SDL_GetMouseState(&x, &y);
				Vector2D tmp(x, y);
				if (t.x == A.x && t.y == A.y) { A = tmp; t = tmp; } else
				if (t.x == B.x && t.y == B.y) { B = tmp; t = tmp; }	else
				if (t.x == C.x && t.y == C.y) { C = tmp; t = tmp; } else
				if (t.x == D.x && t.y == D.y) { D = tmp; t = tmp; }
				SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
				SDL_RenderClear(ren);
				SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
				DrawCurve3(ren, A, B, C, D);
				SDL_RenderPresent(ren);
			}


			if (event.type == SDL_MOUSEBUTTONUP)
			{
				t = NULL;
			}
			
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
		}

	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
