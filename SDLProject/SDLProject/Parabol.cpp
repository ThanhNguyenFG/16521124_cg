#include "Parabol.h"
#include <iostream>
using namespace std;

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);

}

void BresenhamDrawParabolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int w, h;
	SDL_GetRendererOutputSize(ren, &w, &h);
	//cout << w << " " << h << endl;
	// Area 1
	double x = 0, y = 0;
	double p;
	p = -A + 1;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A)
	{
		if (p <= 0)
		{
			p = p + (2 * x + 3);
		}
		else
		{
			p = p - 2 * A + (2 * x + 3);
			y = y + 1;
		}
		x = x + 1;
		Draw2Points(xc, yc, x, y, ren);
	}
	// Area 2
	x = A; y = double(A) / 2;
	Draw2Points(xc, yc, x, y, ren);
	p = -1 + 2 * A;
	Draw2Points(xc, yc, x, y, ren);
	while (yc + y <= h && xc + x <= w && xc + x >= 0 && xc - x >= 0 && xc - x <= w)
	{
		if (p <= 0)
		{
			p = p + 4 * A;
		}
		else
		{
			p = p - 4 * x - 4 + 4 * A;
			x = x + 1;
		}
		y = y + 1;
		Draw2Points(xc, yc, x, y, ren);
	}

}

void BresenhamDrawParabolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int w, h;
	SDL_GetRendererOutputSize(ren, &w, &h);
	// Area 1
	if (A > 0) A = -A;
	double x = 0, y = 0;
	double p;
	p = -A - 1;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= -A)
	{
		if (p > 0)
		{
			p = p - (2 * x + 3);
		}
		else
		{
			p = p - 2 * A - (2 * x + 3);
			y = y - 1;
		}
		x = x + 1;
		Draw2Points(xc, yc, x, y, ren);
	}
	// Area 2
	x = -A; y = double(A) / 2;
	//cout << x << " " << y << endl;
	Draw2Points(xc, yc, x, y, ren);
	p = -1 - 2 * A;
	Draw2Points(xc, yc, x, y, ren);
	while (yc + y <= h && xc + x <= w && xc + x >= 0 && xc - x >= 0 && xc - x <= w)
	{
		if (p <= 0)
		{
			p = p - 4 * A;
		}
		else
		{
			p = p - 4 * x - 4 - 4 * A;
			x = x + 1;
		}
		y = y - 1;
		Draw2Points(xc, yc, x, y, ren);
	}
}
