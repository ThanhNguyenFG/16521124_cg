#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3], y[3];
	float phi = M_PI / 2;
	for (int i = 0; i < 3; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi = phi + 2*M_PI / 3;
	}
	for (int i = 0; i < 2; i++)
	{
		Bresenham_Line(x[i], y[i], x[i + 1], y[i + 1], ren);
	}
	Bresenham_Line(x[2], y[2], x[0], y[0], ren);
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4], y[4];
	float phi = M_PI / 4;
	for (int i = 0; i < 4; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		//cout << x[i] << " " << y[i] << endl;
		phi = phi +  M_PI / 2;
	}
	for (int i = 0; i < 3; i++)
	{
		Bresenham_Line(x[i], y[i], x[i + 1], y[i + 1], ren);
	}
	Bresenham_Line(x[3], y[3], x[0], y[0], ren);
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = M_PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		//cout << x[i] << " " << y[i] << endl;
		phi = phi + 2*M_PI / 5;
	}
	for (int i = 0; i < 4; i++)
	{
		Bresenham_Line(x[i], y[i], x[i + 1], y[i + 1], ren);
	}
	Bresenham_Line(x[4], y[4], x[0], y[0], ren);
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6], y[6];
	float phi = M_PI / 2;
	for (int i = 0; i < 6; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		//cout << x[i] << " " << y[i] << endl;
		phi = phi +  M_PI / 3;
	}
	for (int i = 0; i < 5; i++)
	{
		Bresenham_Line(x[i], y[i], x[i + 1], y[i + 1], ren);
	}
	Bresenham_Line(x[5], y[5], x[0], y[0], ren);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = M_PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		//cout << x[i] << " " << y[i] << endl;
		phi = phi + 4 * M_PI / 5;
	}
	for (int i = 0; i < 4; i++)
	{
		Bresenham_Line(x[i], y[i], x[i + 1], y[i + 1], ren);
	}
	Bresenham_Line(x[4], y[4], x[0], y[0], ren);
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5], xp[5], yp[5];
	float phi = M_PI / 2;
	float phinho = 3 * M_PI / 10;
	float rnho = R * sin(M_PI / 10) / sin(7 * M_PI / 10);
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xp[i] = xc + int(rnho*cos(phinho) + 0.5);
		yp[i] = yc - int(rnho*sin(phinho) + 0.5);
		//cout << x[i] << " " << y[i] << endl;
		phi = phi + 2 * M_PI / 5;
		phinho = phinho + 2 * M_PI / 5;
	}
	for (int i = 0; i < 5; i++)
	{
		Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
	}
	for (int i = 0; i < 4; i++)
	{
		Bresenham_Line(x[i], y[i], xp[i + 1], yp[i + 1], ren);
	}
	Bresenham_Line(x[4], y[4], xp[0], yp[0], ren);
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], y[8], xp[8], yp[8];
	float phi = M_PI / 2;
	float phinho = 3 * M_PI / 8;
	float rnho = R * sin(M_PI / 8) / sin(3 * M_PI / 4);
	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xp[i] = xc + int(rnho*cos(phinho) + 0.5);
		yp[i] = yc - int(rnho*sin(phinho) + 0.5);
		//cout << x[i] << " " << y[i] << endl;
		phi = phi +  M_PI / 4;
		phinho = phinho +  M_PI / 4;
	}
	for (int i = 0; i < 8; i++)
	{
		Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
	}
	for (int i = 0; i < 7; i++)
	{
		Bresenham_Line(x[i], y[i], xp[i + 1], yp[i + 1], ren);
	}
	Bresenham_Line(x[7], y[7], xp[0], yp[0], ren);
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[5], y[5], xp[5], yp[5];
	float phi = startAngle;
	float phinho = startAngle - M_PI / 5;
	float rnho = R * sin(M_PI / 10) / sin(7 * M_PI / 10);
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xp[i] = xc + int(rnho*cos(phinho) + 0.5);
		yp[i] = yc - int(rnho*sin(phinho) + 0.5);
		//cout << x[i] << " " << y[i] << endl;
		phi = phi + 2 * M_PI / 5;
		phinho = phinho + 2 * M_PI / 5;
	}
	for (int i = 0; i < 5; i++)
	{
		Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
	}
	for (int i = 0; i < 4; i++)
	{
		Bresenham_Line(x[i], y[i], xp[i + 1], yp[i + 1], ren);
	}
	Bresenham_Line(x[4], y[4], xp[0], yp[0], ren);
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	int x[5], y[5], xp[5], yp[5];
	float startAngle = M_PI / 2;
	while (r >= 1)
	{
		DrawStarAngle(xc, yc, r, startAngle, ren);
		r = r * sin(M_PI / 10) / sin(7 * M_PI / 10);
		startAngle = startAngle + M_PI;
	}
}
