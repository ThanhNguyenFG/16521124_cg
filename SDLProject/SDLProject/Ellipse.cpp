#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x = 0, y = b;
	int a2 = a * a, b2 = b * b;
	int p;
	p = a2 - 2 * a2 * b + 2 * b2;
	Draw4Points(xc, yc, x, y, ren);
	while (x * x * (a2 + b2) <= a2 * a2)
	{
		if (p > 0)
		{
			p = p + 4 * b2 * x - 4 * a2 * y + 4 * a2 + 6 * b2;
			y = y - 1;
		}
		else
		{
			p = p + 4 * b2 * x + 6 * b2;
		}
		x = x + 1;
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	y = 0; x = a;
	p = b2 - 2 * a * b2 + 2 * a2  ;
	Draw4Points(xc, yc, x, y, ren);
	while (y * y * (a2 + b2) <= b2 * b2)
	{
		if (p > 0)
		{
			p = p + 4 * a2 * y - 4 * b2 * x + 4 * b2 + 6 * a2;
			x = x - 1;
		}
		else
		{
			p = p + 4 * a2 * y + 6 * a2;
		}
		y = y + 1;
		Draw4Points(xc, yc, x, y, ren);
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x = 0, y = b;
	int a2 = a * a, b2 = b * b;
	int p;
	p = 4 * b2 - 4 * a2 * b + a2;
	Draw4Points(xc, yc, x, y, ren);
	while (x * x * (a2 + b2) <= a2 * a2)
	{
		if (p > 0)
		{
			p = p + 8 * b2 * x + 12 * b2 + 8 * a2 - 8 * a2 * y;
			y = y - 1;
		}
		else
		{
			p = p + 8 * b2 * x + 12 * b2;
		}
		x = x + 1;
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	x = a, y = 0;
	p = 4 * a2 - 4 * a * b2 + b2;
	Draw4Points(xc, yc, x, y, ren);
	while (y * y * (a2 + b2) <= b2 * b2)
	{
		if (p > 0)
		{
			p = p + 8 * a2 * y + 12 * a2 + 8 * b2 - 8 * b2 * x;
			x = x - 1;
		}
		else
		{
			p = p + 8 * a2 * y + 12 * a2;
		}
		y = y + 1;
		Draw4Points(xc, yc, x, y, ren);
	}
}